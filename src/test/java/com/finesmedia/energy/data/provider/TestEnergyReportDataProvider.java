package com.finesmedia.energy.data.provider;

import com.finesmedia.energy.energyReport.dto.EnergyReportDtoSet;
import com.finesmedia.energy.energyReport.dto.ImmutableEnergyReportDtoSet;
import com.finesmedia.energy.energyReport.model.AccountTimeSeries;
import com.finesmedia.energy.energyReport.model.EnergyReport;
import com.finesmedia.energy.energyReport.model.HourConsumption;
import com.finesmedia.energy.energyReport.vo.EnergyUnit;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.Collections;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;

public class TestEnergyReportDataProvider {

    public static TestEnergyReportDataProvider create() {
        return new TestEnergyReportDataProvider();
    }

    public EnergyReport getEnergyReport() {
        return new EnergyReport(
            100L,
            new AccountTimeSeries(
                EnergyUnit.KWH,
                "",
                getHourConsumptionList()
            )
        );
    }

    public AccountTimeSeries getAccountTimeSeries() {
        return new AccountTimeSeries(
            getHourConsumptionList()
        );
    }

    public List<HourConsumption> getHourConsumptionList() {
        try {
            return ImmutableList.of(
                HourConsumption.of("2019-12-29T16:00:00+02:00", 0.25),
                HourConsumption.of("2019-12-29T16:00:00+02:00", 0.20),
                HourConsumption.of("2019-12-29T17:00:00+02:00", 0.25),
                HourConsumption.of("2019-12-29T18:00:00+02:00", 0.2),
                HourConsumption.of("2019-12-30T16:00:00+02:00", 0.2),
                HourConsumption.of("2019-12-30T16:00:00+02:00", 0.2),
                HourConsumption.of("2020-01-01T16:00:00+02:00", 0.2),
                HourConsumption.of("2020-01-02T16:00:00+02:00", 0.2),
                HourConsumption.of("2020-01-06T16:00:00+02:00", 0.2),
                HourConsumption.of("2020-02-01T16:00:00+02:00", 0.6)
            );
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public EnergyReportDtoSet getEnergyReportDtoSetByMonth() {
        return ImmutableEnergyReportDtoSet.of(EnergyUnit.KWH,
            ImmutableMap.of(
                "2020-01", 10.450012,
                "2020-02", 145.60
            )
        );
    }
}
