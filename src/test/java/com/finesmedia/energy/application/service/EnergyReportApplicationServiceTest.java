package com.finesmedia.energy.application.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.finesmedia.energy.application.service.dto.EnergyReportDto;
import com.finesmedia.energy.application.service.exception.EnergyReportValidationException;
import com.finesmedia.energy.application.service.query.ImmutableGenerateEnergyReport;
import com.finesmedia.energy.data.provider.TestEnergyReportDataProvider;
import com.finesmedia.energy.energyReport.service.EnergyReportService;
import com.finesmedia.energy.energyReport.vo.AggregateType;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
class EnergyReportApplicationServiceTest {

    private final TestEnergyReportDataProvider dataProvider =
        TestEnergyReportDataProvider.create();

    @Mock
    private EnergyReportService energyReportService;

    @InjectMocks
    private EnergyReportApplicationService energyReportApplicationService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldGenerateEnergyReport() {
        when(energyReportService.getEnergyReport(any()))
            .thenReturn(dataProvider.getEnergyReportDtoSetByMonth());

        List<EnergyReportDto> energyReportDtoList =
            energyReportApplicationService.generateEnergyReport(
                ImmutableGenerateEnergyReport.of(LocalDate.parse("2020-01-01"),
                    LocalDate.parse("2020-02-01"), AggregateType.MONTH, 2.50));

        assertThat(energyReportDtoList.get(0).period()).isEqualTo("2020-01");
        assertThat(energyReportDtoList.get(0).kwh()).isEqualTo(10.45);
        assertThat(energyReportDtoList.get(0).price()).isEqualTo(26.12);

        assertThat(energyReportDtoList.get(1).period()).isEqualTo("2020-02");
        assertThat(energyReportDtoList.get(1).kwh()).isEqualTo(145.6);
        assertThat(energyReportDtoList.get(1).price()).isEqualTo(364.0);
    }

    @Test
    void shouldThrowInvalidDatesException() {
        assertThrows(EnergyReportValidationException.class, () -> {
            energyReportApplicationService.generateEnergyReport(
                ImmutableGenerateEnergyReport.of(LocalDate.now().minusYears(3),
                    LocalDate.now().minusDays(1), AggregateType.MONTH, 2.50));
        });

        assertThrows(EnergyReportValidationException.class, () -> {
            energyReportApplicationService.generateEnergyReport(
                ImmutableGenerateEnergyReport.of(LocalDate.now().minusYears(1),
                    LocalDate.now(), AggregateType.MONTH, 2.50));
        });

        assertThrows(EnergyReportValidationException.class, () -> {
            energyReportApplicationService.generateEnergyReport(
                ImmutableGenerateEnergyReport.of(LocalDate.now().minusDays(5),
                    LocalDate.now().minusDays(10), AggregateType.MONTH, 2.50));
        });
    }
}