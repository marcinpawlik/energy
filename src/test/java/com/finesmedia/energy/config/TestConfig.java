package com.finesmedia.energy.config;

import com.finesmedia.energy.energyReport.repository.EnergyReportRepository;

import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

@ContextConfiguration
public class TestConfig {
    @Bean
    public RestTemplate restTemplate() {
        final RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }

    @Bean
    public EnergyReportRepository energyReportRepository(RestTemplate restTemplate) {
        return new EnergyReportRepository(restTemplate);
    }
}
