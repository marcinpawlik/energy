package com.finesmedia.energy.energyReport.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import com.finesmedia.energy.config.TestConfig;
import com.finesmedia.energy.energyReport.model.EnergyReport;
import com.finesmedia.energy.energyReport.vo.EnergyUnit;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
@ContextConfiguration(classes = TestConfig.class)
class EnergyReportRepositoryTest {

    @Autowired
    private EnergyReportRepository energyReportRepository;

    @Autowired
    private RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    @BeforeEach
    void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    void shouldGetEnergyReport() throws URISyntaxException {

        mockServer.expect(ExpectedCount.once(),
            requestTo(new URI("https://finestmedia.ee/kwh/?start=2020-01-01&end=2020-02-01")))
            .andExpect(method(HttpMethod.GET))
            .andRespond(
                withStatus(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_XML)
                    .body(readResource("energyReport.xml"))
            );

        EnergyReport energyReport =
            energyReportRepository.find(
                LocalDate.parse("2020-01-01"),
                LocalDate.parse("2020-02-01")
            );

        assertThat(energyReport.getId()).isEqualTo(1587624167);
        assertThat(energyReport.getCreatedAt()).isEqualTo(ZonedDateTime.parse("2020-04-23T09:42:47+03:00"));
        assertThat(energyReport.getTimeSeries().getAccountingPoint()).isEqualTo("54EA-5481353548-U");
        assertThat(energyReport.getTimeSeries().getUnit()).isEqualTo(EnergyUnit.KWH);

        assertThat(energyReport.getTimeSeries().getConsumptionHistory().get(0).getTime()).isEqualTo(ZonedDateTime.parse("2019-12-29T16:00:00+02:00"));
        assertThat(energyReport.getTimeSeries().getConsumptionHistory().get(0).getValue()).isEqualTo(0.72d);
        assertThat(energyReport.getTimeSeries().getConsumptionHistory().size()).isEqualTo(824);
    }

    private String readResource(String fileName) {
        try {
            return Resources.toString(Resources.getResource(fileName), Charsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}