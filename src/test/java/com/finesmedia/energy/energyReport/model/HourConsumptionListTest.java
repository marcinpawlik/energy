package com.finesmedia.energy.energyReport.model;

import static org.assertj.core.api.Assertions.assertThat;

import com.finesmedia.energy.data.provider.TestEnergyReportDataProvider;
import com.finesmedia.energy.energyReport.vo.AggregateType;

import java.util.Map;

import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;

class HourConsumptionListTest {
    private static final Offset<Double> EPSILON = Offset.offset(0.000000001);

    private final HourConsumptionList hourConsumptionList = new HourConsumptionList(
        TestEnergyReportDataProvider.create().getHourConsumptionList()
    );

    HourConsumptionListTest() {
    }

    @Test
    void shouldAggregateByDay() {
        Map<String, Double> aggregateByDayResults =
            hourConsumptionList.aggregateBy(AggregateType.DAY);

        assertThat(aggregateByDayResults.get("2019-12-29")).isCloseTo(0.9, EPSILON);
        assertThat(aggregateByDayResults.get("2019-12-30")).isCloseTo(0.4, EPSILON);
        assertThat(aggregateByDayResults.get("2020-01-01")).isCloseTo(0.2, EPSILON);
        assertThat(aggregateByDayResults.get("2020-01-02")).isCloseTo(0.2, EPSILON);
        assertThat(aggregateByDayResults.get("2020-01-06")).isCloseTo(0.2, EPSILON);
        assertThat(aggregateByDayResults.get("2020-02-01")).isCloseTo(0.6,
            EPSILON);

    }

    @Test
    void shouldAggregateByMonth() {

        Map<String, Double> aggregateByMonthResults =
            hourConsumptionList.aggregateBy(AggregateType.MONTH);

        assertThat(aggregateByMonthResults.get("2019-12")).isCloseTo(1.3, EPSILON);
        assertThat(aggregateByMonthResults.get("2020-01")).isCloseTo(0.6,
            EPSILON);
        assertThat(aggregateByMonthResults.get("2020-02")).isCloseTo(0.6,
            EPSILON);
    }
}