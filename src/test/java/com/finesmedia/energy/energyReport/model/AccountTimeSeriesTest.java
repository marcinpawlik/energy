package com.finesmedia.energy.energyReport.model;

import static org.assertj.core.api.Assertions.assertThat;

import com.finesmedia.energy.data.provider.TestEnergyReportDataProvider;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

class AccountTimeSeriesTest {

    private final TestEnergyReportDataProvider dataProvider =
        TestEnergyReportDataProvider.create();

    @Test
    void shouldGetFilteredConsumptionHistory() {
        AccountTimeSeries accountTimeSeries =
            dataProvider.getAccountTimeSeries();

        HourConsumptionList filteredConsumptionHistory = accountTimeSeries.getFilteredConsumptionHistory(
            LocalDate.parse("2020-01-01"),
            LocalDate.parse("2020-02-01")
        );

        assertThat(filteredConsumptionHistory.getConsumptionHistory()
            .get(0).getTime().toString())
            .isEqualTo("2020-01-01T16:00+02:00[GMT+02:00]");
        assertThat(filteredConsumptionHistory.getConsumptionHistory()
            .get(0).getValue()).isEqualTo(0.2);

        assertThat(filteredConsumptionHistory.getConsumptionHistory()
            .get(1).getTime().toString())
            .isEqualTo("2020-01-02T16:00+02:00[GMT+02:00]");
        assertThat(filteredConsumptionHistory.getConsumptionHistory()
            .get(1).getValue()).isEqualTo(0.2);

        assertThat(filteredConsumptionHistory.getConsumptionHistory()
            .get(2).getTime().toString())
            .isEqualTo("2020-01-06T16:00+02:00[GMT+02:00]");
        assertThat(filteredConsumptionHistory.getConsumptionHistory()
            .get(2).getValue()).isEqualTo(0.2);

        assertThat(filteredConsumptionHistory.getConsumptionHistory()
            .get(3).getTime().toString())
            .isEqualTo("2020-02-01T16:00+02:00[GMT+02:00]");
        assertThat(filteredConsumptionHistory.getConsumptionHistory()
            .get(3).getValue()).isEqualTo(0.6);
    }
}