package com.finesmedia.energy.energyReport.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.finesmedia.energy.data.provider.TestEnergyReportDataProvider;
import com.finesmedia.energy.energyReport.dto.EnergyReportDtoSet;
import com.finesmedia.energy.energyReport.query.ImmutableGetEnergyReportQuery;
import com.finesmedia.energy.energyReport.repository.EnergyReportRepository;
import com.finesmedia.energy.energyReport.vo.AggregateType;
import com.finesmedia.energy.energyReport.vo.EnergyUnit;

import java.time.LocalDate;

import org.assertj.core.data.Offset;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
class EnergyReportServiceTest {

    private static final Offset<Double> EPSILON = Offset.offset(0.000000001);

    private final TestEnergyReportDataProvider dataProvider =
        TestEnergyReportDataProvider.create();

    @Mock
    private EnergyReportRepository energyReportRepository;

    @InjectMocks
    private EnergyReportService energyReportService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void shouldGetReportAggregateByDay() {
        when(energyReportRepository.find(any(), any())).thenReturn(
            dataProvider.getEnergyReport()
        );

        EnergyReportDtoSet energyReport = energyReportService.getEnergyReport(ImmutableGetEnergyReportQuery.of(
            LocalDate.parse("2020-01-01"),
            LocalDate.parse("2020-02-01"),
            AggregateType.DAY
        ));

        assertThat(energyReport.energyUnit()).isEqualTo(EnergyUnit.KWH);
        assertThat(energyReport.reports().get("2020-02-01")).isCloseTo(0.6,
            EPSILON);
        assertThat(energyReport.reports().get("2020-01-01")).isCloseTo(0.2,
            EPSILON);
        assertThat(energyReport.reports().get("2020-01-02")).isCloseTo(0.2,
            EPSILON);
        assertThat(energyReport.reports().get("2020-01-06")).isCloseTo(0.2,
            EPSILON);
    }

    @Test
    void shouldGetReportAggregateByMonth() {
        when(energyReportRepository.find(any(), any())).thenReturn(
            dataProvider.getEnergyReport()
        );

        EnergyReportDtoSet energyReport = energyReportService.getEnergyReport(ImmutableGetEnergyReportQuery.of(
            LocalDate.parse("2020-01-01"),
            LocalDate.parse("2020-02-01"),
            AggregateType.MONTH
        ));

        assertThat(energyReport.energyUnit()).isEqualTo(EnergyUnit.KWH);
        assertThat(energyReport.reports().get("2020-02")).isCloseTo(0.6,
            EPSILON);
        assertThat(energyReport.reports().get("2020-01")).isCloseTo(0.6,
            EPSILON);
    }
}