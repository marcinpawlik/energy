package com.finesmedia.energy.application.service.query;

import com.finesmedia.energy.energyReport.vo.AggregateType;

import java.time.LocalDate;

import org.immutables.value.Value;

@Value.Immutable
public interface GenerateEnergyReport {
    @Value.Parameter
    LocalDate startDate();

    @Value.Parameter
    LocalDate endDate();

    @Value.Parameter
    AggregateType aggregateType();

    @Value.Parameter
    Double price();
}
