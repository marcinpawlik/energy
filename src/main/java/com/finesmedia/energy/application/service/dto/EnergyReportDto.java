package com.finesmedia.energy.application.service.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableEnergyReportDto.class)
@JsonDeserialize(as = ImmutableEnergyReportDto.class)
public interface EnergyReportDto {

    @Value.Parameter
    String period();

    @Value.Parameter
    double kwh();

    @Value.Parameter
    double price();

    static EnergyReportDto of(String period, double kwh, double price) {
        return ImmutableEnergyReportDto.of(
            period,
            ((int) (kwh * 100)) / 100.0,
            ((int) (price * 100)) / 100.0
        );
    }
}
