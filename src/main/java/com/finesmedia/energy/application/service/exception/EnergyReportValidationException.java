package com.finesmedia.energy.application.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class EnergyReportValidationException extends RuntimeException {

    public EnergyReportValidationException(String message) {
        super(message);
    }

    public static EnergyReportValidationException invalidEndDate() {
        return new EnergyReportValidationException("endDate must be in the " +
            "range \"today - 2 years\" to today ");
    }

    public static EnergyReportValidationException invalidStartDate() {
        return new EnergyReportValidationException("startDate must be in the " +
            "range \"today - 2 years\" to today ");
    }

    public static EnergyReportValidationException invalidateDates() {
        return new EnergyReportValidationException("endDate must be after startDate");
    }
}
