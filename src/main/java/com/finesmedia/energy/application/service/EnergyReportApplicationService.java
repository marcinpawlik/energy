package com.finesmedia.energy.application.service;

import com.finesmedia.energy.application.service.dto.EnergyReportDto;
import com.finesmedia.energy.application.service.exception.EnergyReportValidationException;
import com.finesmedia.energy.application.service.query.GenerateEnergyReport;
import com.finesmedia.energy.energyReport.dto.EnergyReportDtoSet;
import com.finesmedia.energy.energyReport.query.GetEnergyReportQuery;
import com.finesmedia.energy.energyReport.service.EnergyReportService;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

@Service
public class EnergyReportApplicationService {
    private static final int TWO_YEARS = 2;
    private final EnergyReportService energyReportService;

    public EnergyReportApplicationService(EnergyReportService energyReportService) {
        this.energyReportService = energyReportService;
    }

    public List<EnergyReportDto> generateEnergyReport(GenerateEnergyReport query) {

        LocalDate maxDate = LocalDate.now();
        LocalDate minDate = maxDate.minusYears(TWO_YEARS);

        if (!(query.startDate().isAfter(minDate) && query.startDate().isBefore(maxDate))) {
            throw EnergyReportValidationException.invalidStartDate();
        }

        if (!(query.endDate().isAfter(minDate) && query.endDate().isBefore(maxDate))) {
            throw EnergyReportValidationException.invalidEndDate();
        }

        if (!query.startDate().isBefore(query.endDate())) {
            throw EnergyReportValidationException.invalidateDates();
        }

        EnergyReportDtoSet energyReport =
            energyReportService.getEnergyReport(GetEnergyReportQuery.of(query));

        return energyReport.reports()
            .keySet()
            .stream()
            .map(key -> EnergyReportDto.of(
                key,
                energyReport.reports().get(key),
                energyReport.reports().get(key) * query.price()
            )).collect(Collectors.toList());
    }
}
