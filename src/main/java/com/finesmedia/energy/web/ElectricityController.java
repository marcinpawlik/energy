package com.finesmedia.energy.web;

import com.finesmedia.energy.application.service.EnergyReportApplicationService;
import com.finesmedia.energy.application.service.dto.EnergyReportDto;
import com.finesmedia.energy.application.service.query.ImmutableGenerateEnergyReport;
import com.finesmedia.energy.energyReport.vo.AggregateType;

import java.time.LocalDate;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ElectricityController {

    private final EnergyReportApplicationService energyReportApplicationService;

    public ElectricityController(EnergyReportApplicationService energyReportApplicationService) {
        this.energyReportApplicationService = energyReportApplicationService;
    }

    @GetMapping("/energy-report")
    public List<EnergyReportDto> getEnergyReport(
        @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
        @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
        @RequestParam(name = "aggregateType") AggregateType aggregateType,
        @RequestParam(name = "price", required = false, defaultValue = "0.0") Double price

    ) {
        return energyReportApplicationService.generateEnergyReport(
            ImmutableGenerateEnergyReport.of(startDate, endDate,
                aggregateType, price)
        );
    }
}
