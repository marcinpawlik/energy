package com.finesmedia.energy.energyReport.service;

import com.finesmedia.energy.energyReport.dto.EnergyReportDtoSet;
import com.finesmedia.energy.energyReport.model.EnergyReport;
import com.finesmedia.energy.energyReport.model.HourConsumptionList;
import com.finesmedia.energy.energyReport.query.GetEnergyReportQuery;
import com.finesmedia.energy.energyReport.repository.EnergyReportRepository;

import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class EnergyReportService {
    private final EnergyReportRepository energyReportRepository;

    public EnergyReportService(EnergyReportRepository energyReportRepository) {
        this.energyReportRepository = energyReportRepository;
    }

    public EnergyReportDtoSet getEnergyReport(GetEnergyReportQuery query) {

        EnergyReport energyReport =
            energyReportRepository.find(query.startDate(), query.endDate());

        HourConsumptionList hourConsumptionList = energyReport
            .getTimeSeries()
            .getFilteredConsumptionHistory(query.startDate(), query.endDate());

        Map<String, Double> aggregatedHourConsumption =
            hourConsumptionList.aggregateBy(query.aggregateType());

        return EnergyReportDtoSet.of(
            energyReport.getTimeSeries().getUnit(),
            aggregatedHourConsumption
        );
    }
}
