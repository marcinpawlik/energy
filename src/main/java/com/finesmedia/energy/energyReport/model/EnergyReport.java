package com.finesmedia.energy.energyReport.model;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlRootElement(name = "EnergyReport")
@XmlAccessorType(XmlAccessType.FIELD)
public class EnergyReport implements Serializable {

    private static final long serialVersionUID = 1029701013573781243L;

    @XmlElement(name = "DocumentIdentification", required = true)
    private Long id;

    @XmlElement(name = "DocumentDateTime", required = true)
    @XmlSchemaType(name = "dateTime")
    private XMLGregorianCalendar createdAt;

    @XmlElement(name = "AccountTimeSeries", required = true)
    private AccountTimeSeries timeSeries;

    public EnergyReport(Long id, AccountTimeSeries timeSeries) {
        this.id = id;
        this.timeSeries = timeSeries;
    }

    public EnergyReport() {
    }

    public Long getId() {
        return id;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt.toGregorianCalendar().toZonedDateTime();
    }

    public AccountTimeSeries getTimeSeries() {
        return timeSeries;
    }
}
