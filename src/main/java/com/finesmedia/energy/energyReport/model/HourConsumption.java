package com.finesmedia.energy.energyReport.model;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class HourConsumption implements Serializable {

    private static final long serialVersionUID = 7949693756589186329L;

    @XmlAttribute(name = "ts", required = true)
    @XmlSchemaType(name = "dateTime")
    private XMLGregorianCalendar time;

    @XmlValue
    private Double value;

    public static HourConsumption of(String dateTime, Double value) throws DatatypeConfigurationException {
        return new HourConsumption(DatatypeFactory.newInstance()
            .newXMLGregorianCalendar(dateTime), value);
    }

    public HourConsumption() {
    }

    private HourConsumption(XMLGregorianCalendar time, Double value) {
        this.time = time;
        this.value = value;
    }

    public ZonedDateTime getTime() {
        return time.toGregorianCalendar().toZonedDateTime();
    }

    public Double getValue() {
        return value;
    }
}
