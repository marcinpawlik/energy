package com.finesmedia.energy.energyReport.model;

import com.finesmedia.energy.energyReport.vo.AggregateType;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HourConsumptionList {
    private List<HourConsumption> consumptionHistory;

    public HourConsumptionList(List<HourConsumption> consumptionHistory) {
        this.consumptionHistory = consumptionHistory;
    }

    public List<HourConsumption> getConsumptionHistory() {
        return consumptionHistory;
    }

    public Map<String, Double> aggregateBy(AggregateType aggregateType) {
        return consumptionHistory.stream()
            .collect(
                Collectors.groupingBy(
                    hc -> hc.getTime().format(aggregateType.getFormat()),
                    Collectors.summingDouble(hc -> hc.getValue())
                )
            );
    }
}
