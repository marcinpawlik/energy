package com.finesmedia.energy.energyReport.model;

import com.finesmedia.energy.energyReport.vo.EnergyUnit;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class AccountTimeSeries implements Serializable {

    private static final long serialVersionUID = 2463082874248285981L;

    @XmlElement(name = "MeasurementUnit", required = true)
    private EnergyUnit unit;

    @XmlElement(name = "AccountingPoint", required = true)
    private String accountingPoint;

    @XmlElementWrapper(name = "ConsumptionHistory", required = true)
    @XmlElement(name = "HourConsumption")
    private List<HourConsumption> consumptionHistory;

    public AccountTimeSeries(EnergyUnit unit, String accountingPoint, List<HourConsumption> consumptionHistory) {
        this.unit = unit;
        this.accountingPoint = accountingPoint;
        this.consumptionHistory = consumptionHistory;
    }

    public AccountTimeSeries(List<HourConsumption> consumptionHistory) {
        this.consumptionHistory = consumptionHistory;
    }

    public AccountTimeSeries() {
    }

    public EnergyUnit getUnit() {
        return unit;
    }

    public String getAccountingPoint() {
        return accountingPoint;
    }

    public List<HourConsumption> getConsumptionHistory() {
        return consumptionHistory;
    }

    public HourConsumptionList getFilteredConsumptionHistory(LocalDate start, LocalDate end) {
        List<HourConsumption> filteredConsumptionHistory =
            consumptionHistory.stream()
                .filter(hc ->
                    (
                        hc.getTime().toLocalDate().isAfter(start) ||
                            hc.getTime().toLocalDate().isEqual(start)
                    ) && (
                        hc.getTime().toLocalDate().isBefore(end) ||
                            hc.getTime().toLocalDate().isEqual(end)
                    )
                ).collect(Collectors.toList());

        return new HourConsumptionList(filteredConsumptionHistory);
    }
}
