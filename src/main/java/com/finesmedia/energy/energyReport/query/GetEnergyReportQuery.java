package com.finesmedia.energy.energyReport.query;

import com.finesmedia.energy.application.service.query.GenerateEnergyReport;
import com.finesmedia.energy.energyReport.vo.AggregateType;

import java.time.LocalDate;

import org.immutables.value.Value;

@Value.Immutable
public interface GetEnergyReportQuery {
    @Value.Parameter
    LocalDate startDate();

    @Value.Parameter
    LocalDate endDate();

    @Value.Parameter
    AggregateType aggregateType();

    static GetEnergyReportQuery of(GenerateEnergyReport query) {
        return ImmutableGetEnergyReportQuery.of(query.startDate(),
            query.endDate(), query.aggregateType());
    }
}
