package com.finesmedia.energy.energyReport.repository;

import com.finesmedia.energy.energyReport.model.EnergyReport;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Repository
public class EnergyReportRepository {
    private static final Logger LOG =
        LoggerFactory.getLogger(EnergyReportRepository.class);

    private static final String QUERY_PARAM_START = "start";
    private static final String QUERY_PARAM_END = "end";

    private final RestTemplate restTemplate;

    @Value("${endpoint.energy-report}")
    private String energyReportEndpoint;

    public EnergyReportRepository(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Cacheable("energyReport")
    public EnergyReport find(LocalDate startDate, LocalDate endDate) {
        LOG.info("find energy report by startDate {} endDate {}", startDate, endDate);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(energyReportEndpoint)
            .queryParam(QUERY_PARAM_START,
                startDate.format(DateTimeFormatter.ISO_DATE))
            .queryParam(QUERY_PARAM_END, endDate.format(DateTimeFormatter.ISO_DATE));


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_XML);
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<EnergyReport> response = restTemplate.exchange(
            builder.toUriString(),
            HttpMethod.GET,
            entity, EnergyReport.class);

        return response.getBody();
    }
}
