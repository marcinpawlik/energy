package com.finesmedia.energy.energyReport.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.finesmedia.energy.energyReport.vo.EnergyUnit;

import java.util.Map;

import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableEnergyReportDtoSet.class)
@JsonDeserialize(as = ImmutableEnergyReportDtoSet.class)
public interface EnergyReportDtoSet {

    @Value.Parameter
    EnergyUnit energyUnit();

    @Value.Parameter
    Map<String, Double> reports();

    static EnergyReportDtoSet of(EnergyUnit energyUnit,
                                 Map<String, Double> reports) {
        return ImmutableEnergyReportDtoSet.of(energyUnit, reports);
    }
}
