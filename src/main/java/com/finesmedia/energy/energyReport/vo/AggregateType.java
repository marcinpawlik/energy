package com.finesmedia.energy.energyReport.vo;

import java.time.format.DateTimeFormatter;

public enum AggregateType {
    DAY("yyyy-MM-dd"), WEEK("yyyy-MM-W"), MONTH("yyyy-MM");

    final private String format;

    AggregateType(String format) {
        this.format = format;
    }

    public DateTimeFormatter getFormat() {
        return DateTimeFormatter.ofPattern(format);
    }
}
